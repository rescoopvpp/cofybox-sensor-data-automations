# Zappi EV charger
The Zappi is an EV charger made by Myenergi. Compared to OpenEVSE chargers we have very few of these attached to our boxes. They generally seem to be a bit less troublesome when it comes to data extraction than the OpenEVSE chargers, but they are not without issues.

# Data to be extracted
| Quantity | MQTT output topic | Unit |
| - | - | - |
| Instant charging power | cb/ev/zappi/instant_power | W |
| Energy supplied (session) | cb/ev/zappi/session_energy | kWh |

EV charger metric types were defined by looking at what was available for the OpenEVSE chargers, which differ from the Zappi. While the charging power on the Zappi is just as useful as the combination of current and voltage on the OpenEVSE chargers, we do need to add an appropriate data reporting metric for it.

# Automation operation
The automation runs on a Home Assistant time pattern, triggering once per minute. It finds the data from the Home Assistant states and publishes it on the appropriate MQTT topics.

## The special version for Christian
I've also added a version of the automation for Christian Weingärtner, this one adds the metadata to bypass the glue component. See the "Future" writeup in the root directory of the repo for more information. 

# Session vs. Total energy issues
CoFyCloud expects total charging energy. This is not available for the Zappi, the sensor only provides session charging energy.

# Potential problems
There are some potential difficulties that the Zappis may present, but without access to a limited number of them I don't want to panic just yet.

## Variability in available data
There are currently very few Zappis available to the project. However, from looking at those that we do have available, as well as other Myenergi devices and their literature, it is clear that these devices are intended to be highly customisable compared to the other devices we look at. Users are able to buy extra CT (Current Transformer, for power measurement) clamps and fit them to different parts of the system, potentially leading to situations where two different Zappis are each measuring several extra quantities without any of them being in common. 

Because of this it is better to stick to quantities that are known to be present in all setups - don't get ambitious with data collection. Any state ending in "ct" or "ct1" _should_ be associated with the internal CT clamp of the charger, and be common to every setup.

## Variability in device/HA state names
Looking at the Zappis that are available, we see that they are not necessarily consistent in the format of their state names either. One of the chargers that we have adds an 8-digit number, unique to that charger, in the names of all of the associated states.

Currently, considering the low number of Zappis we have, and the small number of quantities we are interested in, it is not a problem for someone to manually edit the automation so that it accesses the correct state. If more Zappis are added in the future this may need to be addressed, with a blueprint as is used for the OpenEVSE chargers or by using more sophisticated logic in constructing the name of the state to be accessed.