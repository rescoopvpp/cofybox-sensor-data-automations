# Eddi solar power diverter
The Eddi is another Myenergi product. They market it as being used to divert excess energy from PV systems into water heating, where usually it would be sold back to the grid. Therefore we're treating it as an immersion heater for the purposes of data extraction.

# Data to be extracted
| Quantity | MQTT output topic | Unit |
| - | - | - |
| Total active power | cb/heater/eddi/total_active_power | W |
| Session energy | cb/heater/eddi/session_energy | kWh |

We would also like voltage and current data since it's a heater, but the Eddi does not make this available.

# Automation operation
The automation runs on a Home Assistant time pattern, triggering once per minute. It finds the data from the Home Assistant states and publishes it on the appropriate MQTT topics.

# Session vs. Total energy issues
Similarly to the Zappi and several other devices, the Eddi gives us session rather than total energy diverted to the heater. We'd prefer total lifetime energy, not least because the definition of "session" is not well defined in the documentation.

# Potential problems
So far I've only been able to look at one Eddi, so I don't want to declare anything finished, but I also don't have any reason to expect many problems. 

## Variability in device/HA state names
The only problem I suspect we might encounter is something I've noticed on the few Zappis I've looked at, which is differing state names. I'm just going to plagiarise my own writing in the Zappi readme:

"Looking at the Zappis that are available, we see that they are not necessarily consistent in the format of their state names either. One of the chargers that we have adds an 8-digit number, unique to that charger, in the names of all of the associated states.

Currently, considering the low number of Zappis we have, and the small number of quantities we are interested in, it is not a problem for someone to manually edit the automation so that it accesses the correct state. If more Zappis are added in the future this may need to be addressed, with a blueprint as is used for the OpenEVSE chargers or by using more sophisticated logic in constructing the name of the state to be accessed."

Everything that applies to the Zappi applies here, and it's unlikely that this will present any serious issues.