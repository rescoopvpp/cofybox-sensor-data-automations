# OpenEVSE/EmonEVSE
This is one of the more involved/inconvenient sensors to extract data from.

Each OpenEVSE charger has a four character UID. The charger publishes data on MQTT, but it uses the UID in the MQTT topics - for example 'openevse-2b9s/voltage'. Each quantity is published with no metadata on an indvidual topic i.e. voltage, temperature etc. We can extract the data just by editing the glue configuration, but this has to be done for every single box individually because of the UID. Therefore it is more reasonable to ask installers to run a blueprint and use a standard glue config.

## The original method for OpenEVSE data extraction
OpenEVSE/EmonEVSE chargers have been accessible to the project for a long time, and originally data extraction was done by editing the glue config after so that it would listen on the unique MQTT topic that the charger was using.

There are parts of the CoFyBox and CoFyCloud software that were designed around the original way the data extraction worked, and these parts are affected to some degree by changes to the data extraction. See the session vs. total energy section below in particular.

# Data to be extracted
| Quantity | MQTT output topic | Unit |
| ---- | ----------------- | ---- |
|Voltage| cb/ev/openevse/voltage | V |
|Current| cb/ev/openevse/amp | A |
|Energy supplied (session) | cb/ev/openevse/wh | kWh | 

It's worth noting that units are not necessarily consistent between the MQTT and Home Assistant versions of the data.

# Blueprint/Automation operation
Getting this working was a process.

## User setup
Here, we use a blueprint to produce an automation.

The user runs the blueprint, and is asked to select a switch associated with the charger. This is the only input we need from them, so long as the correct switch is selected and the automation is created, it should work.

## Automation flow
The result of the user selection is saved as two variables - one trigger variable and one regular variable. The trigger variable is user to configure the trigger, where we listen for MQTT publications on a topic constructed from the variable.

When the automation is triggered it takes the three quantities by different methods. Voltage is obtained from the payload of the MQTT trigger, while current and energy supplied are obtained from Home Assistant states. The state topics to extract are constructed using the regular variable taken from the user's input.

Finally, the three quantities are rebroadcast on the appropriate generic MQTT topics.

# Why the data extraction works this way
The goal of this blueprint was to make it easier for installers. The thinking is that asking someone to run a blueprint and select from a drop-down list is much more reasonable than going through the whole process of finding the charger's UID and editing the glue config with it. 

There are other ways that this could have been achieved, for example simply providing detailed instructions for installers or using a python script to automatically generate a glue config file for each box. The use of a blueprint and automation was chosen as a compromise between ease of use for installers and consistency with data extraction methods for other sensors.

## Strange looking decisions
Some of the details of the operation of the blueprint/automation do look quite odd.

Initially I tried to trigger the automation on any MQTT message from the charger, and rebroadcast them on known topics. This didn't work because the sensor broadcasts all of its data over MQTT in sets with very little time bewtween each message in the set. The automation would work for the first MQTT message, but wouldn't finish quickly enough to run again for any of the subsequent message in that set.

The ideal solution to this would have been to simply use a time pattern trigger and extract the data from Home Assistant states. The problem with this solution is that we want voltage, current, and charging energy. There is no Home Assistant state for voltage.

The solution to this was to trigger the automation on a voltage MQTT message, and then take the other two quantites from Home Assistant states. 

Why does the user's input get stored as two variables? Home Assistant blueprints have the regular variables section, which is evaluated after the trigger section, and trigger_variables, which is evaluated before the trigger section. This is not well documented and it was only this forum post https://community.home-assistant.io/t/helping-with-trigger-variables-inputs/369180/4 that helped me to get this working. So the trigger variable is used when the user selection sets up the trigger, and the regular variable is used when constructing the state name to pull the data from.

# Session vs. Total energy issues
Another difference between the data in the MQTT and Home Assistant states is the energy injected. The MQTT gives the total energy injected by the charger over the lifetime of the device. The Home Assistant state gives the energy injected during a "session". I had to ask one of the maintainers for the definition of a session, and he said: "A charging session begins when the vehicle is plugged in ends when the vehicle is unplugged. The EVSE can be stopped and started many times during a charging session e.g when using Eco Mode solar PV divert, but the total charging session energy is added up and the session ends when the vehicle is unplugged."

CoFyCloud was designed with the total lifetime energy in mind, and it doesn't deal with session energy well at all. This is so far an unsolved problem, but other devices also only give us session energy, so it may be necessary to extend CoFyCloud to accept both forms.

# Potential improvements
The most obvious improvement, and one that I've been unable to make, would be to control the drop-down menu in the blueprint so that only OpenEVSE devices are shown, to avoid any possibility of mistakes resulting in no data being sent to the cloud.

# Glue example
Note that the energy is being converted from kWh to Wh.