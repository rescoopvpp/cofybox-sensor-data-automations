# Powerfox utility meter
A utility meter mostly used in continental pilot sites. We have a sensor definition for it here https://gitlab.com/rescoopvpp/cofybox-sensor-definitions/-/tree/main/Powerfox.

# Data to be extracted
| Quantity | MQTT output topic | Unit |
| - | - | - |
| Total energy consumed (utility) | cb/utility/powerfox/total_energy_consumed | kWh |
| Total energy injected (utility) | cb/utility/powerfox/total_energy_injected | kWh |
| Total active power (utility) | cb/utility/powerfox/total_active_power | W |
| Total energy injected (solar) | cb/pv/powerfox/total_energy_injected | kWh |
| Total active power (solar) | cb/pv/powerfox/total_active_power | W |

Note that the Powerfox also provides PV data. I've included this in the blueprint and glue keys, and I expect that this will mostly work out of the box, but with some combinations of PV systems and other utility meters/batteries we appear to get duplicate PV from separate sources. In these cases it's probably better for the sake of clarity to use the data coming directly from the PV system.

# Automation operation
The automation runs on a Home Assistant time pattern, triggering once per minute. It finds the data from the Home Assistant states and publishes it on the appropriate MQTT topics.

# Potential problems
Has only been tested on one box so far.