# About this repo
This repo is for me to store Home Assistant automations and blueprints while I do some final checks on them before pushing them to the production cofybox repo. These automations and blueprints are used in the process of extracting data from sensors as putting it into CoFyCloud. It will be organised with one directory per sensor manufacturer, with .yaml files, glue keys and readmes as appropriate for each type of device.

Each sensor works differently and has its own quirks, so each readme will have an explanation of why the extraction works the way it does and what we need to know if we try to change things in the future.

# The general process for data extraction
I'll describe this twice, first backwards and then forwards.

## The backwards version
CoFyCloud takes its data from an MQTT topic that the CoFyBoxes publish called ‘aggregated_data’. This is produced by the cofybox-config service out of a number of MQTT topics. These topics are either produced by the glue service (the older method) or directly by HA automations (the newer method). This data can go directly into the glue service from the sensor over MQTT, or it can go into glue over MQTT via an HA automation, or it can go directly into cofybox-config via HA automation. Different sensors are in different states of development at the moment.

## The forwards version
It's best to see an example. In the original mode of operation, the sensor publishes this:

> openevse-2b9s/voltage 240

openevse-2b9s/voltage is the topic, and 240 is the value.

With some other sensors this data is available as a state in home assistant, rather than over MQTT. In these cases, the solution is to use an automation to publish the data from the state over MQTT. In the older automations in this repo, this is done by publishing on MQTT topics so that it is picked up by glue. Glue knows from the configuration file (`/cofybox-config/glue_keys.json`) to take this MQTT topic, add metadata to it, sometimes do unit conversions or other operations, and republish it on a new topic (also defined in the config) that looks like this:

> data/devices/ev/voltage_phase_l1 {"entity": "ev", "channel": "voltage_phase_l1", "value": 240, "unit": "V", "metric": "electricVehicleCharging", "metricKind": "gauge", "timestamp": "2022-09-30T13:12:25+00:00"}

In some of the most recent automations, we simply bypass the glue service, and produce the above '/data/devices' broadcast in the automation.

The cofybox-config service is subscribing to the 'data/' MQTT topics, and takes the data to reformat and broadcast it into a way that CoFyCloud understands, which looks like this:

> aggregated_data/5c2196b61e3a/mqtt_consumer {"fields":{"value_mean":240},"name":"mqtt_consumer","tags":{"cofybox_id":"f4126fd6c551455ea718560f538c998d","device":"ev","host":"5c2196b61e3a","metric":"electricVehicleCharging","metricKind":"gauge","sensor_id":"ev.voltage_phase_l1","topic":"data/devices/ev/voltage_phase_l1","unit":"V"},"timestamp":1664543700}

Note that aggregated_data goes out every five minutes and reports an average of the previous five minutes of readings.

So long as data is being published on aggregated_data and has the right metadata, CoFyCloud can understand it and graph it. Once you’ve got the aggregated_data set up properly and a box location set, you don’t need to do anything else on the CoFyCloud side.

# Contents of each directory
Each directory in this repo will have whatever files are necessary to extract data from the relevant device or devices, and a readme file explaining how the process works and the thinking behind decisions about that process - some of these are straightforward, some are not.

All files need to have LF line endings when on a CoFyBox.

# The point of these automations
These automations/blueprints are used to get the data into the cofybox-config service, so that the data can then be sent to the cloud. There is some variation in how this is achieved, which is explained by the readme files for each asset type.

# About the MQTT topics the automations use
In cases where the automation bypasses the glue service, they simply use the 'data/devices/' format for MQTT topics that cofybox-config expects.

For the cases where we're using glue, it's useful to have a convention for the MQTT topics that the automations use. MQTT publications created by these automations are begin and end on the CoFyBox, so the top level topic is 'cb'. Following this we go \<device type\>/\<device name\>/\<data type\>, for example 'cb/ev/openevse/voltage'.

(SD)