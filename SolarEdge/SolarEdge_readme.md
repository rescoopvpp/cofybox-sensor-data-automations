# SolarEdge PV system
The SolarEdge is relatively simple for us, since the data is stored in Home Assistant states. 

I have an automation and a blueprint version of this one in the repo, they both work in basically the same way.

# Data to be extracted
| Quantity | MQTT output topic | Unit |
| ---- | ----------------- | ---- |
| Instant power generation | cb/pv/solaredge/total_active_power | W |
| Lifetime energy production | cb/pv/solaredge/total_energy_produced | Wh |

We would also like to be able to read current and voltage, but the Home Assistant integration does not make this information available.

# Automation operation
The automation runs on a Home Assistant time pattern, triggering once per minute. It finds the data from the Home Assistant states and publishes it on the appropriate MQTT topics.

# Potential improvements
This one is probably finished. It is possible that some of the other states made available by the SolarEdge integration would also be of interest to the project.