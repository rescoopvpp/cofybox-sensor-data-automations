# The future of data synchronisation in the project

## The current process

Currently, data is taken from MQTT by the glue component. The data either gets on to MQTT because the sensor/asset publishes it by default, as in the case of the Shellies, or via an HA automation taking state data and publishing it, which is how most of the other sensor/assets work.

Once glue has been set up to take the incoming data, it adds some metadata and formats it correctly, and publishes that payload on a different MQTT topic which is monitored by the cofybox-config component. This component adds some further metadata and republishes it so that it is picked up by CoFyCloud.

## Issues with glue

The glue component is currently not fully maintained, and has a bug in some functionality we'd like to make use of - recipes combining ingredients for a single output (see https://gitlab.com/rescoopvpp/cofybox-balena/-/issues/136) leading to some confusion among pilot sites and the need for workarounds. 

Glue configuration files are also quite fussy, and it's easy to introduce syntax errors while trying to create a file that incorporates a number of different assets. I've done this myself many times while setting things up. If we can do things differently in a way that makes it easier for our pilot sites, this would probably be a better use of time than working on maintaining the glue component.

## Data synchronisation without glue

There is no actual need to have the metadata added by the glue component (or the cofybox-config component for that matter) and Sam's heartbeat automation has worked while bypassing it completely for a while. More recently, Joannes wrote an automation for the Viessmann heat pump that adds the metadata in the automation, which works perfectly. Following this and the issues with glue, it seems that using HA automations to publish MQTT data directly to the cofybox-config component seems like the way forward.

### Implementation

Joannes' Viessmann automation in this repository provides an example of how this would work. I've also included an example automation action here:

```
action:
  - service: mqtt.publish
    data_template:
      topic: data/devices/battery/battery_apparent_output
      payload: >
        {"entity":"battery",
        "metric":"BatteryElectricityPower", "metricKind":"gauge",
        "unit":"W", "friendly_name":"Battery discharge power",
        "value": {{states("sensor.sonnenapiv2_apparent_output") | float | string}},
        "timestamp": {{ now().timestamp() | int | string }},
        "sensorId":"battery.total_active_power"}
```

The same payload we'd get from glue is constructed in the jinja template. It doesn't perfectly map on to the way glue recipes are written; sensorId is \<entity\>.\<channel\> in glue, but there's not much else to watch out for. 

The process for getting data from a sensor/asset we have an automation for then becomes:

1. Ensure the sensor/asset is integrated with Home Assistant.
2. Start the automation. 
3. Set box location in CoFyCloud

This doesn't seem like a large change when written out like this, but I think that the removal of the glue configuration step removes a lot of the friction from the process, especially for the pilot sites.

### Potential issues

The first issue with this, which will hopefully be fixed soon, is that it's vulnerable to the problem we currently have with HA deleting automations for no apparent reason. Of course many of the sensors that we use glue for have the same problem, but it's worth being aware of.

The second issue is with sensors/assets that we just take MQTT directly from. Currently this only really applies to the Shellies. HA automations are not much use when we have batches of MQTT publications coming in with data, this is a problem I had when trying to get the OpenEVSEs working - the automations take too long to execute after the first MQTT message and miss the subsequent ones, and as far as I can tell HA has no way to wait and collect MQTT publications over a time period. The Shellies will have to keep working via glue for the forseeable future.

The third issue is also currently a problem with glue, but it's still worth addressing. The glue recipe functionality is useful in some cases, for example if we're interested in calculating total power usage in a house. We can do this using HA automations and jinja templates, but this requires customisation of the automation and is probably less convenient than if the glue recipes worked as planned. If some functionality could be added on the cloud side we could maintain standardisation of the automations, but I don't know how much extra work this would be for the cloud team.

In my opinion none of these issues should discourage us from going ahead with this change to the way we do data sync, but there are some problems that will need to be ironed out.

## Conclusions

Bypassing glue by adding metadata in HA automations seems like the way to go, although it won't solve all of the problems we have when using glue.

Before anything else I think we need to have a fix for the disappearing automations. After that, I think we need to decide how operations involving multiple sensors are going to work.