# Victron battery system
The Victron battery system is relatively easy to extract data from once it's set up. It communicates over modbus, which requires a configuration file for Home Assistant to access, available here https://gitlab.com/rescoopvpp/cofybox-sensor-definitions/-/tree/main/Victron%20batteries. It's also necessary to add the line `modbus: !include .storage/user/modbus.yaml` to Home Assistant/config/configuration.yaml.

# Data to be extracted
| Quantity | MQTT output topic | Unit |
| - | - | - |
| Battery relative state of charge | cb/battery/victron/rsoc | % |
| Battery voltage | cb/battery/victron/voltage | V |
| Total active battery power | cb/battery/victron/battery_active_power | W |
| Battery consumed Ah | cb/battery/victron/battery_charged_Ah | Ah |
| Power from system to loads | cb/battery/victron/victron_power_to_loads | W |
| Power from grid to system | cb/battery/victron/victron_power_from_grid | W |
| Power from battery to system | cb/battery/victron/battery_power_to_system | W |
| Battery discharged energy | cb/battery/victron/battery_discharged_energy | kWh |
| Battery charged energy | cb/battery/victron/battery_charged_energy | kWh |

Total active power comes from a quantity that the Victron modbus register list calls 'battery power' and is positive when the battery is charging, and negative when it's discharging.

Battery consumed Ah is one that we don't have much documentation for, I'm assuming it means total energy consumed by the battery in the current session. Frustrating that it's given in Ah rather than Wh. 

# Communication failure
I haven't been able to confirm this, but it appears that when Home Assistant can't connect to the Victron battery it will return 0s for the data instead of an error.

# Automation operation
The automation runs on a Home Assistant time pattern, triggering once per minute. It finds the data from the Home Assistant states and publishes it on the appropriate MQTT topics.

# The glue example
I've left it in because I can't comment it out in a .json, but the `battery_energy_charged` ingredient and recipe currently need to be removed for this to work, since Ah is not a permitted unit for glue.

# Modbus configuration
The Victron is accessed with the help of a modbus configuration file as described above. 

The modbus.yaml included in here has been modified to add three quantites, at the bottom of the file: battery power to system, battery energy charged and battery energy discharged.

I don't know if there's a better way to get HA to reload the modbus config file, but rebooting the box works.

# Modbus documentation
For convenience I've included Victron's modbus register list in this directory, which has not been altered except by converting it to the .ods format.

# Potential improvements
This one doesn't require improvement so much as verification - there are a number of quantities that are not particularly well described in Victron's documentation and I'm not 100% sure that they mean what I think they mean.

I've also been assuming that by "system" Victron means the battery, associated control electronics, and any generation equipment connected. I'm not certain that this is correct.

The charged and discharged energy quantities just show as unavailable, which I think may be because I wasn't able to find the correct settings for the modbus connection - the addresses are correct but the slaves may not be.