# Sonnen battery system
Similarly to the Victron batteries, the Sonnen battery system requires an extra set of sensor definitions to work. Rather tha modbus, these work over a REST API, however the end result is quite similar. Matt has written one of these here https://gitlab.com/rescoopvpp/cofybox-sensor-definitions/-/blob/main/Sonnen%20Battery/sensor.yaml using this extremely useful documentation https://jlunz.github.io/homeassistant/#/api/postApiV2SetpointDischargeWatt. 

There's actually three versions of the automation in this directory - the original automation, the blueprint version of that, and then a glue bypass version of the automation. The former two versions work, the glue bypass version throws some kind of unhelpful HA syntax error that I've not been able to track down.

# Data to be extracted
| Quantity | MQTT output topic | Unit |
| ---- | ----------------- | ---- |
| Battery relative state of charge | cb/battery/sonnen/rsoc | % |
| Battery voltage | cb/battery/sonnen/battery_voltage | V |
| Battery apparent power output | cb/battery/sonnen/apparent_output | W |
| Energy discharged by battery | cb/battery/sonnen/powermeter_production | kWh |
| Energy used to charge battery| cb/battery/sonnen/powermeter_consumption | kWh |

# Automation operation
The automation runs on a Home Assistant time pattern, triggering once per minute. It finds the data from the Home Assistant states and publishes it on the appropriate MQTT topics.

The last two quantities in the automation have to be accessed slightly differently because the data is stored as an attribute of the state rather than the state itself, so by consulting the documentation here https://www.home-assistant.io/docs/configuration/state_object/ we see that we can get it using the `state_attr()` function.

## Glue bypass version
We are looking at stopping using glue altogether, as it's not really being maintained at the moment. Joannes has written automations that would include all the metadata we usually add with glue, and this was my test of implementing one myself. It seems to work perfectly well.

# Potential improvements
There are a relatively small number of Sonnens that we've so far been able to look at, and even so there are already some variations in the data that we can actually get out of them. The discharge power/apparent output property so far appears to be available in one of the three boxes, for example. I can't explain this, but it simply be due to different versions of the hardware and software - these differences are not readily visible to me while setting up and testing the automations.

It is possible that some of the other states made available by the Sonnen API would also be of interest to the project.