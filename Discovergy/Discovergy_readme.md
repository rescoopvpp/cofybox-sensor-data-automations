# Discovergy energy meter
A utility meter mostly used in continental pilot sites. This is a different one, this blueprint has been written specifically for the box artsy-sky, and it not likely to be very applicable to any other boxes. 

# Data to be extracted
| Quantity | MQTT output topic | Unit |
| - | - | - |
| 17 kWp PV-Anlange total consumption | cb/utility/discovergy/electricity_rheinstrasse_8_total_consumption_2 | kWh |
| 17 kWp PV-Anlange total power | cb/utility/discovergy/electricity_rheinstrasse_8_total_power_2 | W |
| 17 kWp PV-Anlange total production | cb/utility/discovergy/electricity_rheinstrasse_8_total_production_2 | kWh |
| Summenzähler Hagebutze total consumption | cb/utility/discovergy/electricity_rheinstrasse_2_total_consumption | kWh |
| Summenzähler Hagebutze total power | cb/utility/discovergy/electricity_rheinstrasse_2_total_power | W |
| Summenzähler Hagebutze total production | cb/utility/discovergy/electricity_rheinstrasse_2_total_production | kWh |
| Summenzähler Konvisionär total consumption | cb/utility/discovergy/electricity_rheinstrasse_8_total_consumption | kWh |
| Summenzähler Konvisionär total power | cb/utility/discovergy/electricity_rheinstrasse_8_total_power | W |
| Summenzähler Konvisionär total production | cb/utility/discovergy/electricity_rheinstrasse_8_total_production | kWh |

I (SD) cannot speak German, but my undertstanding is that these are simply three different sites.

# Automation operation
The automation runs on a Home Assistant time pattern, triggering once per minute. It finds the data from the Home Assistant states and publishes it on the appropriate MQTT topics.

# Potential problems
We've been having an issue recently with automations disappearing on some boxes, which we've not yet been able to explain. This issue appears to be affecting this blueprint/automation on artsy-sky. I get an error in HA when logging into the box: 
'Invalid config for [automation]: Failed to load blueprint: Unable to find cofycloud/Discovergy_mqtt_broadcast_blueprint.yaml (See /config/configuration.yaml, line 0).'
So far I've been manually adding the blueprint to the HA /config/blueprints/automation/cofycloud/. The problem doesn't seem to affect the blueprint that we have in the cofybox balena image, so it may be that once this blueprint is part of the balena deployment the problem will stop.